from pathlib import Path

import click

from banqparse.parser import parse_with_llm, parse_csv_mp


@click.group()
def cli():
    pass


@cli.command()
@click.argument("text")
def string(text):
    print("input :")
    print(text)
    tr = parse_with_llm(text)
    print("output :")
    print(tr.__str__())
    click.echo(tr)


@cli.command()
@click.argument("csv_file", type=click.Path(exists=True))
def csv(csv_file):
    main(csv_file)


def main(csv_file):
    with open(csv_file, "r") as f:
        lines = f.readlines()
    # trs, cost = parse_csv(lines)
    trs, cost = parse_csv_mp(lines)
    print(cost)
    for tr in trs:
        print(tr.__str__())


    import pandas as pd
    pre_df = [{'input': l} | tr.dict() for l, tr in zip(lines, trs)]
    df = pd.DataFrame(pre_df)
    df.to_csv("output.csv", index=False)
    print(f"Total cost: {cost}")


if __name__ == "__main__":
    main(Path("../typical_sg_export.csv"))
