from datetime import datetime

from banqparse.transaction import Transaction

EXAMPLES = [
    (
        "03/07/2023;CARTE X6154 REMBT 02/07 SNCF INTERNET COMMERCE ELECTRONIQUE;34,00;EUR;",
        Transaction(
            date_of_transaction=datetime(2023, 7, 2),
            date_on_bank=datetime(2023, 7, 3),
            payee="SNCF",
            amount=34.00,
            currency="EUR",
            category="Transportation",
            description=None,
            payment_method="debit_card",
        )
    ),
    (
        "15/09/2023;VIR INST RE 375890516209 DE: KZEMOS TECHNOLOGIES SL DATE: 15/09/2023 16:48 PROVENANCE: ES Espagne;15,00;EUR;",
        Transaction(
            date_of_transaction=datetime(2023, 9, 15, 16, 48),
            date_on_bank=datetime(2023, 9, 15),
            payee="KZEMOS TECHNOLOGIES SL",
            amount=15.00,
            currency="EUR",
            category=None,
            description="RE 375890516209 (ES)",
            payment_method="bank_transfer",
        )
    ),
    (
        "04/10/2023;PRELEVEMENT EUROPEEN 0301389248 DE: BOUYGUES TELECOM ID: FR35ZZZ418323 MOTIF: 07xxxxx576 REF: PAGP01104X7Z6D;-14,99;EUR;",
        Transaction(
            date_of_transaction=None,
            date_on_bank=datetime(2023, 10, 4),
            payee="BOUYGUES TELECOM",
            amount=-14.99,
            currency="EUR",
            category="Utilities",
            description="tel 07xxxxx576 id FR35ZZZ418323",
            payment_method="direct_debit",
        )
    ),
    (
        "24/04/2023;CARTE X6154 22/04 CHATGPT SUBSCRIPTION 24,00 USD ETATS-UNIS D'AME 1 EUR=1,0939 USD COMMERCE ELECTRONIQUE;-21,94;EUR;",
        Transaction(
            date_of_transaction=datetime(2023, 4, 22),
            date_on_bank=datetime(2023, 4, 24),
            payee="CHATGPT",
            amount=-21.94,
            currency="EUR",
            category="Other",
            description="Subscription 24.00 USD (1 EUR=1.0939 USD)",
            payment_method="debit_card",
        ),
    ),
    (
        "30/08/2023;CARTE X8451 29/08 CAFE GOURMAND S;-0,47;EUR;",
        Transaction(
            date_of_transaction=datetime(2023, 8, 29),
            date_on_bank=datetime(2023, 8, 30),
            payee="Cafe Gourmand",
            amount=-0.47,
            currency="EUR",
            category="Food",
            description=None,
            payment_method="debit_card",
        ),
    ),
]
