from concurrent.futures import ThreadPoolExecutor
from decimal import Decimal, getcontext

from langchain_community.callbacks import get_openai_callback
from langchain_core.language_models import BaseChatModel
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_core.utils.function_calling import tool_example_to_messages
from tqdm import tqdm

from banqparse.transaction import Transaction


def parse_with_llm(transaction_text: str, examples: list[str] = None, llm: BaseChatModel | str = None) -> (
        Transaction, Decimal):
    """Parse a transaction text using a language model.

    Args:
        transaction_text: The text of the transaction to parse.
        examples: A list of examples to help the language model understand the context.
        llm: The language model to use for parsing. Can be a model instance or an openai model name.
                The OPENAI_API_KEY=... environment variable must be set.
    """
    if llm is None:
        llm = "gpt-3.5-turbo"

    if isinstance(llm, str):
        from langchain_openai import ChatOpenAI

        llm = ChatOpenAI(
            model=llm,
            temperature=0,
            seed=0,
        )

    if examples is None:
        examples = []

    prompt = ChatPromptTemplate.from_messages(
        [
            (
                "system",
                "You are an expert extraction algorithm. "
                "Only extract relevant information from the text. "
                "If you do not know the value of an attribute asked "
                "to extract, return null for the attribute's value.",
            ),
            # ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            MessagesPlaceholder("examples"),  # <-- EXAMPLES!
            # ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            ("human", "{text}"),
        ]
    )

    messages = []
    for example_text, tool_call in examples:
        messages.extend(
            tool_example_to_messages(input=example_text, tool_calls=[tool_call])
        )

    runnable = prompt | llm.with_structured_output(
        schema=Transaction,
        method="function_calling",
        include_raw=False,
    )

    getcontext().prec = 6
    with get_openai_callback() as cb:
        # TODO add retries and error handling
        try:
            transaction = runnable.invoke({"text": transaction_text, "examples": messages})
        except Exception as e:
            print("llm error. Transaction blanked.")
            print(e)
            transaction = Transaction()
    return transaction, Decimal(cb.total_cost).quantize(Decimal("0.000001"))


def parse_csv(csv_lines: list[str], examples: list[str] = None, llm=None) -> (
        Transaction, Decimal):
    transactions = []
    total_cost = Decimal(0)
    for line in tqdm(csv_lines):
        transaction, cost = parse_with_llm(line, examples=examples, llm=llm)
        transactions.append(transaction)
        total_cost += cost
    return transactions, total_cost


def parse_csv_mp(csv_lines: list[str], examples: list[str] = None, llm=None) -> (
        list[Transaction], Decimal):
    with ThreadPoolExecutor() as executor:
        futures = [executor.submit(parse_with_llm, line, examples, llm) for line in csv_lines]
        results = [future.result() for future in futures]
    transactions, costs = zip(*results)
    return list(transactions), sum(costs)
