from datetime import datetime
from decimal import Decimal
from enum import Enum
from typing import Optional

from langchain_core.pydantic_v1 import BaseModel, Field

default_categories = [
    "Food",
    "Rent",
    "Utilities",
    "Transportation",
    "Healthcare",
    "Insurance",
    "Education",
    "Entertainment",
    "Clothing",
    "Other",
]


class PaymentMethods(Enum):
    """Payment methods used for transactions."""

    cash = "cash"
    credit_card = "credit_card"
    debit_card = "debit_card"
    direct_debit = "direct_debit"
    bank_transfer = "bank_transfer"
    card_online = "card_online"
    check = "check"
    paypal = "paypal"
    paylib = "paylib"
    other = "other"


Categories = Enum("categories", {category: category for category in default_categories})


class Transaction(BaseModel):
    """Information about a transaction."""

    date_on_bank: Optional[datetime] = Field(None, description="Date when the transaction was recorded in the bank"
                                             )
    date_of_transaction: Optional[datetime] = Field(None, description="Date when the transaction was made"
                                                    )
    payee: Optional[str] = Field(None, description="Organization or person who received the payment"
                                 )
    amount_str: Optional[str] = Field(None, description="Amount of the transaction as a string")
    currency: Optional[str] = Field(None, description="Currency of the transaction")
    category: Optional[Categories] = Field(None, description="Category of the transaction"
                                           )
    description: Optional[str] = Field(None,
                                       description="Details about what the transaction is about. Can be inferred if context allows it."
                                       )
    payment_method: Optional[PaymentMethods] = Field(None, description="Method used to make the payment"
                                                     )

    def __str__(self):
        return f"{self.date_of_transaction} - {self.payee} - {self.amount_str} {self.currency} - {self.category} - {self.payment_method} - {self.description} : {self.date_on_bank}"
