from banqparse import parse_csv_mp, EXAMPLES
from difflib import SequenceMatcher
import pandas as pd


def test_parsing():
    # Setup
    with open("tests/test_input.csv") as f:
        lines = f.readlines()

    # Parse
    transactions, cost = parse_csv_mp(lines, examples=EXAMPLES, llm="gpt-3.5-turbo")

    # Check
    pre_df = [{'input': l} | tr.dict() for l, tr in zip(lines, transactions)]
    df = pd.DataFrame(pre_df)

    df.to_csv("tests/output.csv", index=False)

    text1 = open("tests/output.csv").read()
    text2 = open("tests/test_ref_240609.csv").read()
    m = SequenceMatcher(None, text1, text2)
    print(m.ratio())
    assert m.ratio() > 0.7
